import json
import mysql.connector as sql

print('Por favor inserta las credenciales para conectar con la bd')
host = input('host: ')
user = input('user: ')
password = input('password: ')

conexion = sql.connect(
    host = host,
    user = user,
    password = password
)

cursor = conexion.cursor()

print('Por favor inserta un nombre para la bd a crear')
bd = input('nombre bd: ')

cursor.execute('CREATE DATABASE %s' %bd)

print('Se ha creado la bd %s' %bd)

conexion = sql.connect(
    host = host,
    user = user,
    password = password,
    database = bd
)

cursor = conexion.cursor()

cursor.execute('CREATE TABLE PRODUCTOS (PRODUCTO VARCHAR(100),PRECIO VARCHAR(5))')

print('Se ha creado la tabla PRODUCTOS dentro de la bd')

datos = open('lista.json')
datos = json.load(datos)
lista = []

for j in datos:
    lista.append([j.get('producto'),j.get('precio')])

query = "INSERT INTO PRODUCTOS (PRODUCTO, PRECIO) VALUES (%s, %s)"

for j in lista:
    valores = (j[0],j[1])
    cursor.execute(query,valores)

conexion.commit()

print('Se han insertado los datos extraidos en la tabla PRODUCTOS')
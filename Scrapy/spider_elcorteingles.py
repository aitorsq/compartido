import scrapy

class MySpider(scrapy.Spider):
    name = 'elcorteingles'
    start_urls = [
        'https://www.elcorteingles.es/electronica/accesorios-informatica/',
    ]

    def parse(self, response):
        item = 0
        for j in response.css('#products-list > ul').css('li.products_list-item'):
            item += 1
            yield {
                'producto': response.xpath('//*[@id="products-list"]/ul/li[%s]/div/div/div[1]/div[2]/div[1]/div[1]/div[1]/h2/a/p/text()' %item ).get(),
                'precio': response.xpath('//*[@id="products-list"]/ul/li[%s]/div/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/span/span[1]/text()' %item).get()
            }